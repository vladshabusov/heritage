'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
const jsf = require('json-schema-faker');
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  catalog: getBooks,
  book: getBook,
  home: getHome
};

var schema = {
  "type": "array",
  "minItems": 9,
  "maxItems": 9,
  "items": {
    "type": "object",
    "required": [
      "id", "name", "author", "image", "price", "faculty"
    ],
    "properties": {
      "id": {
        "type": "string",
        "faker": "random.number"
      },
      "name": {
        "type": "string",
        "faker": "commerce.productName"
      },
      "author": {
        "type": "string",
        "chance": "name"
      },
      "image": {
        "type": "string",
        "faker": "image.image"
      },
      "price": {
        "type": "string",
        "chance": "dollar"
      },
      "faculty": {
        "type": "string",
        "faker": "company.companyName"
      }
    }
  }
}

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getBooks(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  //var faculty = req.swagger.params.faculty.value || 'MMF';
  //var hello = util.format('Hello, %s!', name);

  // this sends back a JSON response which is a single string
  jsf.resolve(schema).then(sample => res.json(sample));
}

/*var schemaBook = {
  "type": "array",
  "minItems": 1,
  "maxItems": 1,
  "items": {
    "type": "object",
    "required": [
      "id", "name", "author", "image", "year", "price", "faculty", "descriptions", "comments"
    ],
    "properties": {
      "id": {
        "type": "string",
        "faker": "random.number"
      },
      "name": {
        "type": "string",
        "faker": "commerce.productName"
      },
      "author": {
        "type": "string",
        "chance": "name"
      },
      "image": {
        "type": "string",
        "faker": "image.image"
      },
      "year": {
        "type": "string",
        "chance": "year"
      },
      "price": {
        "type": "string",
        "chance": "dollar"
      },
      "faculty": {
        "type": "string",
        "faker": "company.companyName"
      },
      "descriptions": {
        "type": "string",
        "faker": "lorem.paragraphs"
      },
      "comments": {
        "type": "array",
        "minItems": 2,
        "maxItems": 4,
        "items": {
          "type": "object",
          "required": [
            "user", "image", "text", "rate"
          ],
          "properties": {
            "user": {
              "type": "string",
              "chance": "name"
            },
            "image": {
              "type": "string",
              "faker": "image.image"
            },
            "text": {
              "type": "string",
              "faker": "lorem.paragraph"
            },
            "rate": {
              "type": "string",
              "chance": "hour"
              }
            }
          }
      }
    }
  }
}*/

function getBook(req, res) {
  //var id = req.swagger.params.id.value || '00001';
  //var title = util.format('Hey %s', date);

res.json([
    {
      "id": '' + faker.random.number(),
      "name": '' + faker.commerce.productName(),
      "author": '' + faker.name.firstName() + ' ' + faker.name.lastName(),
      "image": '' + faker.image.image(),
      "year": '' + faker.finance.amount(1900,2021,0),
      "price": '$' + faker.finance.amount(3,20,2),
      "faculty": '' + faker.company.companyName() + ' Faculty',
      "descriptions": '' + faker.lorem.paragraphs() ,
      "comments": [
        {
        "user": '' + faker.name.firstName() + ' ' + faker.name.lastName(),
        "image": '' + faker.image.image(),
        "text": '' + faker.lorem.paragraph(),
        "rate": '' + faker.finance.amount(3,5,0)
      },
      {
        "user": '' + faker.name.firstName() + ' ' + faker.name.lastName(),
        "image": '' + faker.image.image(),
        "text": '' + faker.lorem.paragraph() ,
        "rate": '' + faker.finance.amount(3,5,0)
      },
      {
        "user": '' + faker.name.firstName() + ' ' + faker.name.lastName(),
        "image": '' + faker.image.image(),
        "text": '' + faker.lorem.paragraph() ,
        "rate": '' + faker.finance.amount(2,5,0)
      }
      ],
    }
  ]);

 /* jsf.resolve(schemaBook).then(sample => res.json(sample));*/
}

function getHome(req, res) {
  //var id = req.swagger.params.id.value || '00001';
  //var title = util.format('Hey %s', date);
  res.json([
    {
      "id": '' + faker.random.number(),
      "name": '' + faker.commerce.productName(),
      "author": '' + faker.name.firstName() + ' ' + faker.name.lastName(),
      "image": '' + faker.image.image(),
      "year": '' + faker.finance.amount(1900,2021,0),
      "price": '$' + faker.finance.amount(3,20,2),
      "faculty": '' + faker.company.companyName() + ' Faculty',
      "descriptions": '' + faker.lorem.paragraphs()
    }
  ]);
}