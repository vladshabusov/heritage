/* tslint:disable */
/* eslint-disable */
/**
 * Books service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Comment,
    CommentFromJSON,
    CommentFromJSONTyped,
    CommentToJSON,
} from './';

/**
 * 
 * @export
 * @interface Book
 */
export interface Book {
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    id: string;
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    author: string;
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    image: string;
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    year: string;
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    price: string;
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    faculty: string;
    /**
     * 
     * @type {string}
     * @memberof Book
     */
    descriptions: string;
    /**
     * 
     * @type {Array<Comment>}
     * @memberof Book
     */
    comments?: Array<Comment>;
}

export function BookFromJSON(json: any): Book {
    return BookFromJSONTyped(json, false);
}

export function BookFromJSONTyped(json: any, ignoreDiscriminator: boolean): Book {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': json['id'],
        'name': json['name'],
        'author': json['author'],
        'image': json['image'],
        'year': json['year'],
        'price': json['price'],
        'faculty': json['faculty'],
        'descriptions': json['descriptions'],
        'comments': !exists(json, 'comments') ? undefined : ((json['comments'] as Array<any>).map(CommentFromJSON)),
    };
}

export function BookToJSON(value?: Book | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'name': value.name,
        'author': value.author,
        'image': value.image,
        'year': value.year,
        'price': value.price,
        'faculty': value.faculty,
        'descriptions': value.descriptions,
        'comments': value.comments === undefined ? undefined : ((value.comments as Array<any>).map(CommentToJSON)),
    };
}


