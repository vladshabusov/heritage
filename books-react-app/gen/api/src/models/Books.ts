/* tslint:disable */
/* eslint-disable */
/**
 * Books service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Books
 */
export interface Books {
    /**
     * 
     * @type {string}
     * @memberof Books
     */
    id: string;
    /**
     * 
     * @type {string}
     * @memberof Books
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof Books
     */
    author: string;
    /**
     * 
     * @type {string}
     * @memberof Books
     */
    image: string;
    /**
     * 
     * @type {string}
     * @memberof Books
     */
    price: string;
    /**
     * 
     * @type {string}
     * @memberof Books
     */
    faculty: string;
}

export function BooksFromJSON(json: any): Books {
    return BooksFromJSONTyped(json, false);
}

export function BooksFromJSONTyped(json: any, ignoreDiscriminator: boolean): Books {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': json['id'],
        'name': json['name'],
        'author': json['author'],
        'image': json['image'],
        'price': json['price'],
        'faculty': json['faculty'],
    };
}

export function BooksToJSON(value?: Books | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'name': value.name,
        'author': value.author,
        'image': value.image,
        'price': value.price,
        'faculty': value.faculty,
    };
}


