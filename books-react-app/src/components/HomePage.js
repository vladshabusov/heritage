import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';

const api = new Api.DefaultApi()

class HomePage extends React.Component {

    constructor(props) {
        super(props);
        const faculty = 'MMF';

        this.state = {
            home: [],
            faculty: faculty
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(catalog) {
        const response = await api.home({ faculty: this.state.faculty });
        this.setState({ home: response });
     }


    render() {
        return <div class="filter">
            <div><img src="/images/logo.png" class = "logo_home" alt="логотип!" />
            <br />
            <span class="logo_span">Books and notes to prepare for the session</span>
            </div>
            <div class="catalog_osnova_home">
                {this.state.home.map(
                    (book) =>
                    <div>
                        <div aligh="left" class="book_home" >
                            <img src={book.image} alt="book" class="book_img_home" /><br />
                            <div class="book_info_home">
                            <span class="book_name_home">{book.name}</span><br />
                            <span class="book_author_home">{book.author}</span><br />
                            <span class="book_prise_home">{book.price}</span><br />
                            <span class="book_descriptions_home">{book.descriptions}</span><br />
                            <a href="/catalog/:id" class="button_more">
                                <button class="popular_book" type="submit">More detailed</button>
                            </a>
                            </div>
                            
                        </div>
                    </div>)}
                
                   
            </div>
            <div class="About_us">
            <div class = "about_us_info">
                <span class="name_about_us">About us</span>
                <hr class="line_about_us" />
                
                <span class="describtion_about_us">
                This online store is designed to help students.<br />
                With our service, it will be easier for them to prepare for exams<br />
                and tests, because the most useful information is here.<br />
                Also, you can easily and simply check the level<br />
                of knowledge in various fields with the help of tests.<br />
                <br />
                This site will also be useful for teachers, as it makes it<br />
                easier to find the necessary literature.<br />
                <br />
                Here you can find books and summaries of various<br />
                specialties and subjects.<br />
                </span>
            </div>
                <img src="/images/about_us.png" class="about_us_img"  />
            </div> 

            <div class="clanding">
            <span class="name_clanding">Catalog</span>
            <br />
            <div class="catalog">
            <div class="catalog_element"><img src="/images/book.png" class="catalog_img" />
            <span class="catalog_img_name">Books and textbooks</span>
            <button class="catalog_buttom">More detailed</button>
            </div>
            <div class="catalog_element"><img src="/images/conspect.png"class="catalog_img" />
            <span class="catalog_img_name">Synopsis</span>
            <button class="catalog_buttom">More detailed</button>
            </div>
            <div class="catalog_element"><img src="/images/pol_infa.png"class="catalog_img" />
            <span class="catalog_img_name">Useful articles</span>
            <button class="catalog_buttom">More detailed</button>
            </div>
            </div>
            </div>
        </div>
    }
}

export default withRouter(HomePage);