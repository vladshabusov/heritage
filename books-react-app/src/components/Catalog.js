import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';
import CurrentBook from './CurrentBook';

const api = new Api.DefaultApi()

class Catalog extends React.Component {

    constructor(props) {
        super(props);
        const faculty = 'MMF';

        this.state = {
            catalog: [],
            faculty: faculty
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(catalog) {
        const response = await api.catalog({ faculty: this.state.faculty });
        this.setState({ catalog: response });
     }


    render() {
        return <div class="filter">
            <h2>Catalog of books</h2>
            <div class="osnova">
            <fieldset class="filter_checkbox">
                <div class="Checkbox_field">
                <legend aligh="left"><b>Faculty</b><p class="dop_infa">The university in the example is considered BSU</p></legend>
                <p><input type="checkbox" class="checkbox" />
                    <label class="check">Faculty of Biology</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Military Faculty</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Faculty of Geography and Geoinformatics</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Faculty of Socio-Cultural Communications</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Faculty of History</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Faculty of Journalism</label><br />
                    <button class="more" type="submit">Show 14 more</button></p>
                

                <legend><b>Specialty</b><p class="dop_infa">Depends on the choice of faculty<br />
                                       In the example, the Faculty of Mechanics and Mathematics</p></legend>
                <p> <input type="checkbox"  name="1"/> <label for="lastname">Mathematics and IT technology</label><br />
                    <input type="checkbox" name="1" /> <label for="lastname">Scientific and Pedagogical activity</label><br />
                    <input type="checkbox" name="1" /> <label for="lastname">Scientific and Industrial activity</label><br />
                    <input type="checkbox" name="1" /> <label for="lastname">Scientific and Design activity</label><br />
                    <input type="checkbox" name="1" /> <label for="lastname">Scientific and Economic activity</label><br />
                    <input type="checkbox" name="1" /> <label for="lastname">Computer Mathematics and Systems Analysis</label><br />
                    <input type="checkbox" name="1" /> <label for="lastname">Mechanics and Mathematical modeling</label></p>
        
                <legend aligh="left"><b>Course</b></legend>
                <p> <input type="checkbox" class="checkbox" />
                    <label class="check">1</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">2</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">3</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">4</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">5</label><br /></p> 

                <legend aligh="left"><b>Subject</b></legend>
                <p> <input type="checkbox" class="checkbox" />
                    <label class="check">Analytical geometry</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Algebra and number theory</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Web programming</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Programming</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Mathematical analysis</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">History</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Information technology</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">Political Science</label><br />
                    <input type="checkbox" class="checkbox" />
                    <label class="check">English language</label><br />
                    <button class="more" type="submit">Show more</button></p>
                    <button class="filter_end" type="submit">Apply a filter</button>
                    </div>
            </fieldset>
            <div class="catalog_osnova">
                {this.state.catalog.map(
                    (book) =>
                    <div>
                       <div align="center" class="book" >
                            <img src={book.image} width="290px" height="290px" alt="book" /><br />
                            <span class = "book_name">{book.name}</span><br />
                            <span class="book_author">{book.author}</span><br />
                            <div class="price_button_div">
                               <span>{book.price}</span>
                               <a href="/catalog/:id" class="button_more">
                                    <button class="popular_book" type="submit">More detailed</button>
                                </a>
                            </div>
                        </div>
                    </div>)}
            </div>
            </div>
        </div>
    }
}

export default withRouter(Catalog);