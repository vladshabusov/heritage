import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';

const api = new Api.DefaultApi()

class CurrentBook extends React.Component {

    constructor(props) {
        super(props);
        const id = '00001';

        this.state = {
            book: [],
            id: id
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(book) {
            const response = await api.book({ id: this.state.id });
            this.setState({ book: response });
        }


    render() {
        return <div class="filter">
           <div class="catalog_osnova">
                {this.state.book.map(
                    (book) =>
                    <div>
                        <div align="center"class="one_book" >
                            <img src={book.image} width="850px" height="480px" alt="book" /><br />
                            <div class ="one_book_info__1">
                            <span class="book__name">{book.name}, {book.year}</span><span class="book_dop_infa">{book.id} views</span><br />
                            <span class="book__author">{book.author}</span><br />
                            <div class="book_prise">{book.price}</div><br />
                            <div class="book_descriptions">{book.descriptions}</div><br />
                            </div>
                            <div class="button"><button class="buy_book">Buy a book</button></div>
                            <div class="one_book_info__2">
                           
                            <h2 class="reviews_h2">Reviews</h2>
                            {/* Отдается список комментариев */}
                            {this.state.book[0].comments.map( (book) =>
                            <ul class="comments">
                                <li class="user__info" >
                                <img src={book.image} width="50" height="50" class="user_photo" />
                                <div class="user_dop_infa">
                                <b>{book.user}</b>
                                <div class="book_rate"> {book.rate} star(s)</div>
                                </div>  
                                <div class="book_text">{book.text}</div>
                                </li><br />
                            </ul>
                            )} {/* Конец комментариев */}
                            </div>
                        </div>
                    </div>)}
            </div>
        </div>
    }
}

export default withRouter(CurrentBook);