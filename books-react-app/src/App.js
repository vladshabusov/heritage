import './App.css';
import Catalog from './components/Catalog';
import CurrentBook from './components/CurrentBook';
import HomePage from './components/HomePage';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,  
  useLocation
} from "react-router-dom";

function App() {
  return (

  <Router>
  <div>
    <div id="nav">
          <Link to="/home"><img class="logo" src="/images/logo.png" alt="логотип!" /></Link>
          <ul>
            <li><Link to="/home">Home</Link></li>
            <li><Link to="/catalog">Catalog</Link></li>
            <li><Link to="/">Tests</Link></li>
            <li><Link to="/">Question-Answer</Link></li>
            <li><Link to="/about">About</Link></li>     
            <li><Link to="/home">Contacts</Link></li>
          </ul>
          <div id="lk">
          <Link to="/" class="podat"> Post an ad </Link>
          <Link to="/" class="ico"><img src="/images/lk.jpg" alt="личный кабинет" /></Link>
          <Link to="/" class="ico"><img src="/images/basket.jpg" alt="корзина" /></Link>
          </div>
    </div>
          <form id="form1">
            <input id="poisk" type="text" name="Поиск по сайту" placeholder=" Search" />
            <Link to="/"><img src="/images/lupa.png" alt="значок поиска" /></Link>
          </form>

      {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
    <div className="App">
       
      <section> 
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/catalog/:id">
            <CurrentBook />
          </Route>
          <Route path="/catalog">
            <Catalog />
          </Route>
          <Route path="/home">
            <HomePage />
          </Route>
          <Route path="*">
          <Redirect to={`/home/`} />
          </Route>
        </Switch>
      </section>
    </div>
  </div>
  </Router>
);
}

function About() {

return (
  <div>
    <h2>
      About us
    </h2>
    <p>Here is the description of the service and necessary terms.</p>
  </div>
);
}

export default App;
